%-----------------------------------------------------------------------------------------------------------------------------------------------%
%	The MIT License (MIT)
%
%	Copyright (c) 2015 Jan Küster
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%	
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%	
%
%-----------------------------------------------------------------------------------------------------------------------------------------------%


%============================================================================%
%
%	DOCUMENT DEFINITION
%
%============================================================================%

%we use article class because we want to fully customize the page and dont use a cv template
\documentclass[10pt,A4]{article}	


%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

%we use utf8 since we want to build from any machine
\usepackage[utf8]{inputenc}		

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xifthen}

%----------------------------------------------------------------------------------------
%	FONT
%----------------------------------------------------------------------------------------
% Fontawesome
\usepackage{fontawesome5}
\usepackage{romannum}
\usepackage{amsmath,amsfonts,amssymb}
% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
\usepackage[default]{raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
%\usepackage[thin]{roboto} 

% set font default
\renewcommand*\familydefault{\sfdefault} 	
\usepackage[T1]{fontenc}

% more font size definitions
\usepackage{moresize}		


%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

%debug page outer frames
%\usepackage{showframe}			


%define page styles using geometry
\usepackage[a4paper]{geometry}		

% for example, change the margins to 2 inches all round
\geometry{top=1.75cm, bottom=-.6cm, left=1.5cm, right=1.5cm} 	

%use customized header
\usepackage{fancyhdr}				
\pagestyle{fancy}

%less space between header and content
\setlength{\headheight}{-5pt}		


%customize entries left, center and right
\lhead{}
\chead{ \small{Mir Sakhawat Hossain  $\cdot$ Research Consultant $\cdot$ Dhaka, Bangladesh $\cdot$  \textcolor{sectcol}{\textbf{s.hossain18@gmail.com}}  $\cdot$ +8801726801630}}
\rhead{}


%indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for layouting tables
\usepackage{multicol}			
\usepackage{multirow}

%extended aligning of tabular cells
\usepackage{array}

\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0pt}}p{#1}}%


%----------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for header image
\usepackage{graphicx}

%for floating figures
\usepackage{wrapfig}
\usepackage{float}
%\floatstyle{boxed} 
%\restylefloat{figure}

%for drawing graphics		
\usepackage{tikz}				
\usetikzlibrary{shapes, backgrounds,mindmap, trees}


%----------------------------------------------------------------------------------------
%	Color DEFINITIONS
%---------------------------------------------------------------------------------------- 

\usepackage{color}

%accent color
\definecolor{sectcol}{RGB}{255,150,0}

%dark background color
\definecolor{bgcol}{RGB}{110,110,110}

%light background / accent color
\definecolor{softcol}{RGB}{225,225,225}


%============================================================================%
%
%
%	DEFINITIONS
%
%
%============================================================================%

%----------------------------------------------------------------------------------------
% 	HEADER
%----------------------------------------------------------------------------------------

% remove top header line
\renewcommand{\headrulewidth}{0pt} 

%remove botttom header line
\renewcommand{\footrulewidth}{0pt}	  	

%remove pagenum
\renewcommand{\thepage}{}	

%remove section num		
\renewcommand{\thesection}{}			

%----------------------------------------------------------------------------------------
% 	ARROW GRAPHICS in Tikz
%----------------------------------------------------------------------------------------

% a six pointed arrow poiting to the left
\newcommand{\tzlarrow}{(0,0) -- (0.2,0) -- (0.3,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.1,0.2) -- cycle;}	

% include the left arrow into a tikz picture
% param1: fill color
%
\newcommand{\larrow}[1]
{\begin{tikzpicture}[scale=0.58]
	 \filldraw[fill=#1!100,draw=#1!100!black]  \tzlarrow
 \end{tikzpicture}
}

% a six pointed arrow poiting to the right
\newcommand{\tzrarrow}{ (0,0.2) -- (0.1,0) -- (0.3,0) -- (0.2,0.2) -- (0.3,0.4) -- (0.1,0.4) -- cycle;}

% include the right arrow into a tikz picture
% param1: fill color
%
\newcommand{\rarrow}
{\begin{tikzpicture}[scale=0.7]
	\filldraw[fill=sectcol!100,draw=sectcol!100!black] \tzrarrow
 \end{tikzpicture}
}



%----------------------------------------------------------------------------------------
%	custom sections
%----------------------------------------------------------------------------------------

% create a coloured box with arrow and title as cv section headline
% param 1: section title
%
\newcommand{\cvsection}[1]
{
\colorbox{sectcol}{\mystrut \makebox[1\linewidth][l]{
\larrow{bgcol} \hspace{-8pt} \larrow{bgcol} \hspace{-8pt} \larrow{bgcol} \textcolor{white}{\textbf{#1}}\hspace{4pt}
}}\\
}

%create a coloured arrow with title as cv meta section section
% param 1: meta section title
%
\newcommand{\metasection}[2]
{
\begin{tabular*}{1\textwidth}{p{2.4cm} p{11cm}}
\larrow{bgcol}	\normalsize{\textcolor{sectcol}{#1}}&#2\\[12pt]
\end{tabular*}
}

%----------------------------------------------------------------------------------------
%	 CV EVENT
%----------------------------------------------------------------------------------------

% creates a stretched box as cv entry headline followed by two paragraphs about 
% the work you did
% param 1:	event time i.e. 2014 or 2011-2014 etc.
% param 2:	event name (what did you do?)
% param 3:	institution (where did you work / study)
% param 4:	what was your position
% param 5:	some words about your contributions
%
\newcommand{\cvevent}[5]
{
\vspace{8pt}
	\begin{tabular*}{1\textwidth}{p{2.3cm}  p{10.8cm} x{3.9cm}}
 \textcolor{bgcol}{#1}& \textbf{#2} & \vspace{2.5pt}\textcolor{sectcol}{#3}

	\end{tabular*}
\vspace{-12pt}
\textcolor{softcol}{\hrule}
\vspace{6pt}
	\begin{tabular*}{1\textwidth}{p{2.3cm} p{14.4cm}}
&		 \larrow{bgcol}  #4\\[3pt]
&		 \larrow{bgcol}  #5\\[6pt]
	\end{tabular*}

}

% creates a stretched box as 
\newcommand{\cveventmeta}[2]
{
	\mbox{\mystrut \hspace{87pt}\textit{#1}}\\
	#2
}

%----------------------------------------------------------------------------------------
% CUSTOM STRUT FOR EMPTY BOXES
%----------------------------------------- -----------------------------------------------
\newcommand{\mystrut}{\rule[-.3\baselineskip]{0pt}{\baselineskip}}

%----------------------------------------------------------------------------------------
% CUSTOM LOREM IPSUM
%----------------------------------------------------------------------------------------
\newcommand{\lorem}
{Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus.}



%============================================================================%
%
%
%
%	DOCUMENT CONTENT
%
%
%
%============================================================================%
\begin{document}


%use our custom fancy header definitions
\pagestyle{fancy}	


%---------------------------------------------------------------------------------------
%	TITLE HEADLINE
%----------------------------------------------------------------------------------------
\vspace{-20.55pt}

% use this for multiple words like working titles etc.
%\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\hspace{46pt}\HUGE{\textcolor{white}{\textsc{Jan Küster}} } \textcolor{sectcol}{\rule[-1mm]{1mm}{0.9cm}} \parbox[b]{5cm}{   \large{ \textcolor{white}{{IT Consultant}}}\\
% \large{ \textcolor{white}{{Resume}}}}
%}}

% use this for single words, e.g. CV or RESUME etc.
\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\HUGE{\textcolor{white}{\textsc{Mir Sakhawat Hossain}} } \textcolor{sectcol}{\rule[-1mm]{1mm}{0.9cm}} \HUGE{\textcolor{white}{\textsc{CV}} } }}


%----------------------------------------------------------------------------------------
%	HEADER IMAGE
%----------------------------------------------------------------------------------------

\begin{figure}[H]
\begin{flushright}
	%\includegraphics[trim= 320 130 460 210,clip,width=0.2\linewidth]{myfoto.jpg}	%trimming relative to image size!
	\includegraphics[clip,width=0.2\linewidth]{profile_pic.png}
\end{flushright}
\end{figure}

%---------------------------------------------------------------------------------------
%	QR CODE (optional)
%----------------------------------------------------------------------------------------
%\vspace{-136pt}
%\hspace{0.75\linewidth}
%\includegraphics[width=103pt]{qrcode}
%\normalsize
%\vspace{88pt}

%---------------------------------------------------------------------------------------
%	META SECTION
%----------------------------------------------------------------------------------------

\vspace{-114pt}

\metasection{Status:}{M.Sc. Mathematics}
\metasection{Fields:}{Academic Teaching, Geographic Information Services (GIS)} 
\metasection{Skils:}{Linux, Data Science, Machine Learning, Python, FORTRAN, C, Git, Mathematica, MATLAB}
\metasection{Languages:}{English (Fluent) and Bengali (Fluent)}
\metasection{Professional Membership:}{IEEE Computer Society, International Astrostatistics Association}

\vspace{6pt}

%---------------------------------------------------------------------------------------
%	Research Interest
%----------------------------------------------------------------------------------------
\cvsection{Research Interest}\\
Distant Galaxies, Galaxy Clusters, Dark Matter in Stellar and Galactic Systems, Gravitational Lensing (Strong and Weak), Black Holes, Active Galactic Nuclei, Supernovae, Cosmological Parameters, Cosmic Microwave Background and Exoplanet
%---------------------------------------------------------------------------------------
%	SUMMARAY (optional)
%----------------------------------------------------------------------------------------

%\cvsection{Summary}\\
%I am a digital media graduate (M.Sc.) with project experience in educational research as well as in the private sector. During my studies I focused on e-assessment software and moved over to b2b software for IBM Notes Domino.

%Currently I develop and evaluate the next generation learning management system with Meteor based on an extensive nursing curriculum for healthcare education.  I also love fitness, martial arts, videogames, news and Sci-Fi series.\\[-2pt]

%============================================================================%
%
%	CV SECTIONS AND EVENTS (MAIN CONTENT)
%
%============================================================================%




%---------------------------------------------------------------------------------------
%	EDUCATION SECTION
%--------------------------------------------------------------------------------------
\cvsection{Education}

\cvevent{2014 - 2015}{Masters of Science}{Kabi Nazrul Government College}{Majoring in Mathematics}{CGPA 2.67 out of 4}

%\textcolor{softcol}{\hrule}

%
\cvevent{2009 - 2014}{Bachelor of Science}{National University of Bangladesh}{Majoring in Mathematics}{CGPA 3.01 out of 4}

%\textcolor{softcol}{\hrule}

%
\cvevent{2006 - 2007}{Higher Secondary Certificate (HSC)}{Dhaka Imperial College}{Science Group}{GPA 4.50 out of 5}

%\textcolor{softcol}{\hrule}

%
\cvevent{2004 - 2005}{Secondary School Certificate (SSC)}{Dhaka Collegiate School}{Science Group}{GPA 4.88 out of 5}

\cvevent{2021}{Data-Driven Astronomy}{Coursera}{Completed within 23 hours (approximately)}{Learned about Computational thinking and Statistical Methods to solve Astronomical problems}

\cvevent{2020}{Analyzing the Universe}{Coursera}{Completed within 21 hours (approximately)}{Learned about how to analyze energy spectra and time series data in X-ray Astronomy}
\pagebreak
%---------------------------------------------------------------------------------------
%	Relevant Coursework
%----------------------------------------------------------------------------------------
\cvsection{Relevant Coursework}
%
\cvevent{\faUniversity}{Bachelor of Science in Mathematics}{Total Credit- $128$}{Calculus-\Romannum{1} (3753), Linear Algebra (3754), Analytic and Vector Geometry (3755), Ordinary Differential Equations (3762), Computer Programming (3763), Calculus-\Romannum{2} (7373), Abstract Algebra (3772), Real Analysis (3773), Numerical Analysis (3774), Complex Analysis (3775), Differential Geometry (3776), Mechanics (3777), Linear Programming (3778), Theory of Numbers (3782), Methods of Applied Mathematics (3783), Tensor Analysis (3784), Partial Differential Equations (3785), Hydrodynamics (3786), Discrete Mathematics (3789), Modeling in Biology (3791), Topology and Functional Analysis (3792)}{Physics-\Romannum{1} (6272), Physics-\Romannum{2} (6273), Chemistry-\Romannum{1} (6282), General Chemistry-\Romannum{2} (7282), Physics-\Romannum{3} (7273), Physics-\Romannum{4} (7276), Environmental Chemistry (7283)}

\cvevent{\faUniversity}{Masters of Science in Mathematics}{Total Credit- $36$}{Advanced Number Theory (313701), Theory of Rings and Modules (313705), }{ Theory of Relativity (313713),  Differential and Integral Equations (313715), Geometry of Differential Manifolds (313721)}




%---------------------------------------------------------------------------------------
%	EXPERIENCE
%----------------------------------------------------------------------------------------
\cvsection{Experience}

%
\cvevent{2016}{Mathematics and Science Teacher}{City International School and College}{Identifying and sequencing key concepts and skills to be taught. Developing a variety of teaching methods and activities to support different learning styles}{Integrating real-world examples and applications to make learning relevant and meaningful. Differentiating instruction to provide support for struggling learners and challenge advanced learners}

%\textcolor{softcol}{\hrule}

%
\cvevent{2021 - Present}{Research Consultant of GIS Division}{CEGIS}{Collecting and aggregating data from a variety of sources, such as sensors, satellites, and aerial imagery. Cleaning, organizing, and storing geospatial data in databases}{Performing spatial analysis to identify patterns and trends in the data. Creating maps, charts, and other visualizations to communicate the results of the analysis}


%\textcolor{softcol}{\hrule}

%
%\cvevent{2012 - 2014}{Scientific Employee / Software Development}{University of Bremen}{Invented a flexible assessment framework, targeting industrial trainees}{Supervised software development lifecycle, Recruited team members}

%\textcolor{softcol}{\hrule}

%
%\cvevent{2011 / 11}{Project Management Simulation Training}{Getoq Consulting}{Performed a two-day project simulation from management perspective}{Topics included customer contracts, change management, controlling, operational tasks}

%\textcolor{softcol}{\hrule}


%

%-------------------------------------------------------------------------------------------------
%	Publications 
%--------------------------------------------------------------------------------------------------

\cvsection{Publications}
\\
\cvevent{2018}{SALSA is an ICT based educational tool for Astrophysics students to study structure and dynamics of Milky Way Galaxy}{IEEE}{SALSA is a low-cost radio telescope system that can be used by astrophysics students to study the structure and dynamics of the Milky Way Galaxy. SALSA is an ICT-based tool, meaning that it can be remotely controlled over the internet, making it a valuable resource for students who do not have access to traditional observatories.}{We studied and analyzed the data that was taken from SALSA to prove its effectiveness}

\cvevent{2023}{Machine Learning Approaches for Classification and Diameter Prediction of Asteroids}{Springer Nature Singapur}{The paper  proposes using machine learning algorithms to classify asteroids and predict their diameter.}{We evaluated several machine learning algorithms and found that XGBoost performed best on both tasks.}

%-------------------------------------------------------------------------------------------------
%	Outreach 
%--------------------------------------------------------------------------------------------------
\cvsection{Outreach}
\cvevent{2019 - 2020}{Academic Volunteer}{International Astronomical Union}{Developing educational materials and resources that are tailored to the needs of different audiences. Used my expertise in the field of astronomy to support the IAU's research and advocacy efforts}{Used my multilingual skills to translate educational materials into other languages. Used my teaching experience to develop and deliver educational activities and workshops.}


%-------------------------------------------------------------------------------------------------
%	ARTIFICIAL FOOTER (fancy footer cannot exceed linewidth) 
%--------------------------------------------------------------------------------------------------

\null
\vspace*{\fill}
\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\mystrut \small \faGlobe \textcolor{white}{www.mirsakhawat.com} $\cdot$ \faLinkedin \textcolor{white}{https://bd.linkedin.com/in/mirsakhawathossain}}}




%============================================================================%
%
%
%
%	DOCUMENT END
%
%
%
%============================================================================%
\end{document}
